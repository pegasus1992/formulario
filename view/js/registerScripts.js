/*document.getElementById("submit").onclick = function () {
    var tipoDoc = document.getElementById("selectTipoDoc").value;
    var numDoc = document.getElementById("numDoc").value;
    
    var nombre1 = document.getElementById("name1").value;
    var nombre2 = document.getElementById("name2").value;
    var apellido1 = document.getElementById("surname1").value;
    var apellido2 = document.getElementById("surname2").value;
    
    var pais = document.getElementById("pais").value;
    var entidad = document.getElementById("entidad").value;
    var encargo = document.getElementById("encargo").value;
    
    var estudios = document.getElementByName("estudios").value;
    
    var correo = document.getElementById("correo").value;
    var pass = document.getElementById("password").value;
    var repass = document.getElementById("repassword").value;
    
    if (isNotNull(tipoDoc) && isNotNull(numDoc) && isNotNull(nombre1) 
            && isNotNull(apellido1) && isNotNull(apellido2) 
            && isNotNull(pais) && isNotNull(entidad) && isNotNull(encargo) && isNotNull(estudios) 
            && isNotNull(correo) && isNotNull(pass) && isNotNull(repass)) {
        document.getElementById("password").value = sha1(sha1(pass));
        document.getElementById("repassword").value = sha1(sha1(repass));
    }
};*/

function activateFields() {
    document.getElementById("estudio1").removeAttribute("disabled");
    document.getElementById("estudio2").removeAttribute("disabled");
    document.getElementById("estudio3").removeAttribute("disabled");
    document.getElementById("estudio4").removeAttribute("disabled");
    document.getElementById("estudio5").removeAttribute("disabled");
}

function desactivateFields() {
    document.getElementById("estudio1").setAttribute("disabled", "true");
    document.getElementById("estudio2").setAttribute("disabled", "true");
    document.getElementById("estudio3").setAttribute("disabled", "true");
    document.getElementById("estudio4").setAttribute("disabled", "true");
    document.getElementById("estudio5").setAttribute("disabled", "true");
}