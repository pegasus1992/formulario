 $(document).ready(function() {
     $(function() {
         $.datepicker.setDefaults($.datepicker.regional["es"]);
         var config = {
             firstDay: 0,
             closeText: 'Cerrar',
             gotoCurrent: false,
             currentText: "Hoy",
             dateFormat: "yy-mm-dd",
             monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
             monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
             dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
             dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
             dayNamesShort: [ "Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab" ],
             weekHeader: 'Sm',
             isRTL: false,
             showMonthAfterYear: false,
             yearSuffix: '',
             hideIfNoPrevNext: true,
             minDate: 'today'
         };
         $(".fecha").datepicker( config );
         $.datepicker.setDefaults($.datepicker.regional['es']);
     });
 });