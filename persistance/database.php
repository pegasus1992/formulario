<?php
include_once 'utils/constantes.php';

/**
 * Archivo encargado de realizar la conexión a la BD.
 * Autor: Julián González Prieto (Avuuna, la Luz del Alba).
 */

/**
 * Permite conectar a la BD.
 * @return type Devuelve la conexion establecida.
 */
function connect() {
    return mysqli_connect(URL_SQL, USER_SQL, PASS_SQL, BD_SQL);
}

/**
 * Ejecuta un comando sql.
 * @param type $sql
 * @return boolean Devuelve true si se pudo ejecutar el comando, false de lo contrario.
 */
function executeSimpleQuery($sql) {
    $pdo = connect();
    $pdo->autocommit(false);
    $result = $pdo->query($sql);
    $resp = false;
    if($result === false) {
        $pdo->rollback();
    } else {
        $pdo->commit();
        $resp = true;
    }
    $pdo->close();
    return $resp;
}

/**
 * Ejecuta un comando sql.
 * @param type $sql
 * @return boolean Devuelve true si se pudo ejecutar el comando, false de lo contrario.
 */
function executeQueryCommit($pdo, $sql) {
    $result = $pdo->query($sql);
    $resp = false;
    if($result === false) {
        $pdo->rollback();
    } else {
        $pdo->commit();
        $resp = true;
    }
    $pdo->close();
    return $resp;
}

/**
 * Ejecuta un comando sql sin hacer commit.
 * @param type $sql
 * @return boolean Devuelve true si se pudo ejecutar el comando, false de lo contrario.
 */
function executeQueryNoCommit($pdo, $sql) {
    $result = $pdo->query($sql);
    $resp = false;
    if($result === false) {
    } else {
        $resp = true;
    }
    return $resp;
}

/**
 * Realiza la consulta a la BD.
 * @param type $sql
 * @return type
 */
function getResultSetNoCommit($pdo, $sql) {
    $result = $pdo->query($sql);
    return $result;
}

function connectPDO() {
    $pdo = connect();
    $pdo->autocommit(false);
    return $pdo;
}

/**
 * Ejecuta una serie de comandos sql.
 * @param type $sqls
 * @return boolean Devuelve true si se pudieron ejecutar todos los comandos, false de lo contrario.
 */
function executeMultiQuery($sqls) {
    $pdo = connect();
    $pdo->autocommit(false);
    $ok = true;
    foreach ($sqls as $sql) {
        $result = $pdo->query($sql);
        if($result === false) {
            $ok = false;
            break;
        }
    }
    
    $resp = false;
    if($ok === false) {
        $pdo->rollback();
    } else {
        $pdo->commit();
        $resp = true;
    }
    $pdo->close();
    return $resp;
}

/**
 * Realiza la consulta a la BD.
 * @param type $sql
 * @return type
 */
function getResultSet($sql) {
    $pdo = connect();
    $result = $pdo->query($sql);
    $pdo->close();
    return $result;
}