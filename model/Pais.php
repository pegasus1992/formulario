<?php
include_once 'persistance/database.php';

class Pais {
    private $id, $nombre, $estado;
    
    public function Pais($id, $nombre, $estado) {
        $this->setId($id);
        $this->setNombre($nombre);
        $this->setEstado($estado);
    }
    
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    function getEstado() {
        return $this->estado;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }
    
    /**
     * Trae de la BD los paises activos registrados.
     * @return \Pais
     */
    public static function getPaisesActivos() {
        $filtro = "WHERE estado = 'A'";
        return Pais::traerPaises($filtro);
    }
    
    /**
     * Trae de la BD todos los paises registrados.
     * @return \Pais
     */
    public static function getPaises() {
        return Pais::traerPaises();
    }
    
    public static function getPais($id) {
        $filtro = "WHERE id = $id";
        return Pais::traerPaises($filtro)[0];
    }
    
    /**
     * Trae los paises registrados de la BD.
     * @param type $filtro Filtro de busqueda de países.
     * @return \Pais
     */
    private static function traerPaises($filtro = "") {
        $sql = "SELECT * FROM Pais ";
        $result = getResultSet($sql.$filtro);
        $paises = array();
        if($result->num_rows > 0) {
            while($fila = mysqli_fetch_array($result)) {
                $pais = new Pais($fila['id'], $fila['nombre'], $fila['estado']);
                $paises[] = $pais;
            }
        }
        return $paises;
    }
}