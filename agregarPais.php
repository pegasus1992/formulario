<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/agregarPaisController.php';

if(isset($_SESSION['email'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Agregar País</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Agregar País</h4>
                <div>
                    <label>Nombre:</label>
                    <input placeholder="Ingrese el nombre del país" name="nombre" id="nombre" type="text" autocomplete="off" required/>
                </div>
                <div align="center">
                    <input name="register" id="submit" type="submit" value="Registrar" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
            </form>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}