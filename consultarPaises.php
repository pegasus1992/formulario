<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarPaisesController.php';

if(isset($_SESSION['email'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Consultar Países</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <div style="width:550px" class="texto2">
                <h4 align="center">Consultar Países</h4>
                <table border="0" class="asociados">
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                    </tr>
                    <?php
                    $paises = Pais::getPaises();
                    foreach($paises as $pais) {
                        echo '<tr><form action="" method="get">'
                        . '<td align="center"><input type="text" name="id" style="width:100px" value="' . $pais->getId() . '" readonly></td>'
                        . '<td align="center"><input type="text" name="nombre" style="width:200px" value="' . $pais->getNombre() . '" readonly></td>';
                        if($pais->getEstado() == "A") {
                            echo '<td align="center"><input name="modificar" type="submit" value="Modificar" class="Button"></td>'
                            . '<td align="center"><input name="desactivar" type="submit" value="Desactivar" class="Button"></td>';
                        } else {
                            echo '<td align="center"></td>'
                            . '<td align="center"><input name="reactivar" type="submit" value="Reactivar" class="Button"></td>';
                        }
                        echo '</form></tr>';
                    }
                    ?>
                </table>
                <br/>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
                <br/>
            </div>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}