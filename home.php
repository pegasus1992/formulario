<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'utils/funciones.php';
include_once 'controller/loginController.php';

if(isset($_SESSION['email'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Home Page</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div align="center" style="width:800px" class="texto2">
            <h1 class="Titulo">Página de Inicio</h1>
        </div>
        <div style="width:500px" class="texto2">
            <?php
            echo '<p class="LinkLogin">Bienvenido, '.$_SESSION['nombre'].'</p>';
            ?>
            <a href="logout.php" class="LinkLogin3">Cerrar Sesión</a>
            <br/>
            <br/>
        </div>
        <br/>
        <br/>
        <?php
        if($_SESSION['rol'] == "A") {
            ?>
        <div style="width:350px" class="texto2">
            <h4>Catálogo de Países</h4>
            <input type="button" class="Button2" value="Agregar uno" onclick="window.location='agregarPais.php'">
            <input type="button" class="Button2" value="Consultar todos" onclick="window.location='consultarPaises.php'">
            <br/>
            <br/>
        </div>
            <?php
        }
        ?>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}