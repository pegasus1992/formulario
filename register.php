<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/registerController.php';
include_once 'model/Pais.php';

if(!isset($_SESSION['email'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Página de Registro</title>';
        ?>
        <script src="view/js/encryption.js" type="text/javascript"></script>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <form action="" method="post" class="registro">
                <h4 align="center">Página de Registro</h4>
                <div>
                    <label>Tipo de Documento (*):</label>
                    <select name="selectTipoDoc" id="selectTipoDoc" required>
                        <option></option>
                        <option value="CC">Cédula de Ciudadanía</option>
                        <option value="CE">Cédula de Extranjería</option>
                    </select>
                </div>
                <div>
                    <label>Numero de Documento (*):</label>
                    <input placeholder="Ingrese su número de documento" name="numDoc" id="numDoc" type="text" autocomplete="off" maxlength="15" required/>
                </div>
                <br/>
                <div>
                    <label>Nombres (*):</label>
                    <input placeholder="Ingrese su primer nombre" name="name1" id="name1" type="text" autocomplete="off" required/>
                </div>
                <div>
                    <input placeholder="Ingrese su segundo nombre" name="name2" id="name2" type="text" autocomplete="off"/>
                </div>
                <br/>
                <div>
                    <label>Apellidos (*):</label>
                    <input placeholder="Ingrese su primer apellido" name="surname1" id="surname1" type="text" autocomplete="off" required/>
                </div>
                <div>
                    <input placeholder="Ingrese su segundo apellido" name="surname2" id="surname2" type="text" autocomplete="off" required/>
                </div>
                <br/>
                <div>
                    <label>Nacionalidad (*):</label>
                    <select name="pais" id="pais" required>
                        <option></option>
                        <?php
                        $paises = Pais::getPaisesActivos();
                        foreach($paises as $pais) {
                            echo '<option value="'.$pais->getId().'">'.$pais->getNombre().'</option>';
                        }
                        ?>
                    </select>
                </div>
                <div>
                    <label>Entidad (*):</label>
                    <input placeholder="Ingrese la entidad en la que labora" name="entidad" id="entidad" type="text" autocomplete="off" required/>
                </div>
                <div>
                    <label>Cargo (*):</label>
                    <input placeholder="Ingrese el cargo que desempeña en la entidad" name="encargo" id="encargo" type="text" autocomplete="off" required/>
                </div>
                <br/>
                <div>
                    <label>Estudios (*):</label>
                    <!--input placeholder="Ingrese el cargo que desempeña en la entidad" name="encargo" id="encargo" type="text" autocomplete="off" required/-->
                    <radio>
                        <input type="radio" name="estudios" value="Primaria" required onchange="desactivateFields()"/>Primaria
                        <input type="radio" name="estudios" value="Secundaria" required onchange="desactivateFields()"/>Secundaria
                        <br/>
                        <input type="radio" name="estudios" value="Superior" required onchange="desactivateFields()"/>Superior
                        <input type="radio" name="estudios" value="Otros" required onchange="activateFields()"/>Otros
                    </radio>
                </div>
                <div>
                    <input name="estudio1" id="estudio1" type="text" autocomplete="off" disabled/>
                </div>
                <div>
                    <input name="estudio2" id="estudio2" type="text" autocomplete="off" disabled/>
                </div>
                <div>
                    <input name="estudio3" id="estudio3" type="text" autocomplete="off" disabled/>
                </div>
                <div>
                    <input name="estudio4" id="estudio4" type="text" autocomplete="off" disabled/>
                </div>
                <div>
                    <input name="estudio5" id="estudio5" type="text" autocomplete="off" disabled/>
                </div>
                <br/>
                <div>
                    <label>Teléfono de contacto:</label>
                    <input placeholder="Ingrese su teléfono de contacto" name="telefono" id="telefono" type="text" autocomplete="off"/>
                </div>
                <div>
                    <label>Celular de contacto:</label>
                    <input placeholder="Ingrese su celular de contacto" name="celular" id="celular" type="text" autocomplete="off"/>
                </div>
                <br/>
                <div>
                    <label for="correo">Correo (*):</label>
                    <input placeholder="Ingrese su correo" type="email" name="correo" id="correo" autocomplete="off" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$" required/>
                </div>
                <div>
                    <label for="password">Contraseña (*):</label>
                    <input placeholder="Ingrese su contraseña" name="password" id="password" type="password" value="" autocomplete="off" required/>
                </div>
                <div>
                    <label for="repassword">Repetir contraseña (*):</label>
                    <input placeholder="Repita su contraseña" name="repassword" id="repassword" type="password" value="" autocomplete="off" required/>
                </div>
                <div align="center">
                    <input name="register" id="submit" type="submit" value="Registrar" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
            </form>
        </div>
        <!-- REGISTER SCRIPTS -->
        <script src="view/js/registerScripts.js" type="text/javascript"></script>
    </body>
</html>
    <?php
} else {
    redirigir('home.php');
}