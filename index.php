<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/loginController.php';

if(!isset($_SESSION['email'])) {
   ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Iniciar Sesión</title>';
        ?>
        <!--link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script-->
        <script src="view/js/encryption.js" type="text/javascript"></script>
        <link href="view/css/index.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <form action="" method="post" class="login">
            <h4 align="center">Iniciar Sesión</h4>
            <div>
                <label>Correo:</label>
                <input placeholder="Ingrese su correo" name="correo" id="correo" type="email" autocomplete="off" required/>
            </div>
            <div>
                <label>Contraseña:</label>
                <input placeholder="Ingrese su contraseña" name="password" id="password" type="password" value="" autocomplete="off" required/>
            </div>
            <div align="center">
                <input name="login" id="submit" type="submit" value="Ingresar" class="Button2"/>
                <input name="register" type="button" value="Registrarse" onclick="window.location='register.php'" class="Button2">
            </div>
        </form>
        <!-- LOGIN SCRIPTS -->
        <script src="view/js/loginScripts.js" type="text/javascript"></script>
    </body>
</html>
    <?php
} else {
    redirigir('home.php');
}
