<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';

if(isset($_POST['register'])) {
    $tipoDoc = $_POST['selectTipoDoc'];
    $numeroDoc = $_POST['numDoc'];
    
    $nombre1 = $_POST['name1'];
    $nombre2 = $_POST['name2'];
    $apellido1 = $_POST['surname1'];
    $apellido2 = $_POST['surname2'];
    
    $nacionalidad = $_POST['pais'];
    $entidad = $_POST['entidad'];
    $encargo = $_POST['encargo'];
    
    $estudiosRadio = $_POST['estudios'];
    $estudio1 = null;
    $estudio2 = null;
    $estudio3 = null;
    $estudio4 = null;
    $estudio5 = null;
    if($estudiosRadio == "Otros") {
        $estudio1 = $_POST['estudio1'];
        $estudio2 = $_POST['estudio2'];
        $estudio3 = $_POST['estudio3'];
        $estudio4 = $_POST['estudio4'];
        $estudio5 = $_POST['estudio5'];
    }
    
    $telefono = $_POST['telefono'];
    $celular = $_POST['celular'];
    
    $correo = strtolower($_POST['correo']);
    $password = sha1(sha1($_POST['password']));
    $repassword = sha1(sha1($_POST['repassword']));
    
    if($password == $repassword) {
        $sql = "SELECT DISTINCT * FROM Usuario WHERE email = '$correo' OR (tid = '$tipoDoc' AND nid = '$numeroDoc')";
        $result = getResultSet($sql);
        
        if($result->num_rows == 0) {
            $sqlRegistro = "INSERT INTO Registro (email, clave) VALUES ('$correo', '$password')";
            
            $tables = array("tid", "nid", "nombre1", "apellido1", "apellido2", "nacionalidad", "entidad", "encargo", "email");
            $values = array("'$tipoDoc'", "'$numeroDoc'", "'$nombre1'", "'$apellido1'", "'$apellido2'", "$nacionalidad", "'$entidad'", "'$encargo'", "'$correo'");
            
            if($nombre2 != null) {
                $tables[] = "nombre2";
                $values[] = "'$nombre2'";
            }
            
            $insertIntoUsuario = array2String($tables);
            $insertValuesUsuario = array2String($values);
            $sqlUsuario = "INSERT INTO Usuario ($insertIntoUsuario) VALUES ($insertValuesUsuario)";
            
            $sqlEstudios = null;
            if($estudiosRadio == "Otros") {
                $valuesEstudios = array();
                if($estudio1 != null) {
                    $valuesEstudios[] = "('$tipoDoc', '$numeroDoc', '$estudio1')";
                }
                if($estudio2 != null) {
                    $valuesEstudios[] = "('$tipoDoc', '$numeroDoc', '$estudio2')";
                }
                if($estudio3 != null) {
                    $valuesEstudios[] = "('$tipoDoc', '$numeroDoc', '$estudio3')";
                }
                if($estudio4 != null) {
                    $valuesEstudios[] = "('$tipoDoc', '$numeroDoc', '$estudio4')";
                }
                if($estudio5 != null) {
                    $valuesEstudios[] = "('$tipoDoc', '$numeroDoc', '$estudio5')";
                }
                $insertValuesEstudios = array2String($valuesEstudios);
                $sqlEstudios = "INSERT INTO Estudios (tid, nid, estudio) VALUES $insertValuesEstudios";
            } else {
                $sqlEstudios = "INSERT INTO Estudios (tid, nid, estudio) VALUES ('$tipoDoc', '$numeroDoc', '$estudiosRadio')";
            }
            
            $sqlTelefonos = null;
            if($telefono != null || $celular != null) {
                $valuesTelefonos = array();
                if($telefono != null) {
                    $valuesTelefonos[] = "('$tipoDoc', '$numeroDoc', $telefono)";
                }
                if($celular != null) {
                    $valuesTelefonos[] = "('$tipoDoc', '$numeroDoc', $celular)";
                }
                $insertValuesTelefonos = array2String($valuesTelefonos);
                $sqlTelefonos = "INSERT INTO Telefonos (tid, nid, telefono) VALUES $insertValuesTelefonos";
            }
            
            $sqls = array($sqlRegistro, $sqlUsuario, $sqlEstudios);
            if($sqlTelefonos != null) {
                $sqls[] = $sqlTelefonos;
            }
            
            if(executeMultiQuery($sqls)) {
                $_SESSION['email'] = $correo;
                $_SESSION['nombre'] = $nombre1 . ($nombre2 != null ? " " . $nombre2 : "") . " " . $apellido1 . " " . $apellido2;
                $_SESSION['rol'] = "U";
                redirigir('home.php');
            }
        }
    }
}