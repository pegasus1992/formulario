<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';
include_once 'model/Pais.php';

if(isset($_GET['desactivar'])) {
    $update = "UPDATE Pais SET estado = 'I' WHERE id = '" . $_GET['id'] . "'";
    if(executeSimpleQuery($update)) {
        redirigir("consultarPaises.php");
    }
} else if(isset($_GET['reactivar'])) {
    $update = "UPDATE Pais SET estado = 'A' WHERE id = '" . $_GET['id'] . "'";
    if(executeSimpleQuery($update)) {
        redirigir("consultarPaises.php");
    }
} else if(isset($_GET['modificar'])) {
    redirigir("modificarPais.php?id=".$_GET['id']);
}