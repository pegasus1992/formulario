<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/modificarPaisController.php';
include_once 'model/Pais.php';

if(isset($_SESSION['email'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Modificar País</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <?php
            $pais = Pais::getPais($_GET['id']);
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Modificar País</h4>
                <div>
                    <label>ID:</label>
                    <?php echo '<input name="id" id="id" type="text" value="'.$pais->getId().'" readonly/>' ?>
                </div>
                <div>
                    <label>Nombre:</label>
                    <?php echo '<input placeholder="Ingrese el nombre del país" name="nombre" id="nombre" type="text" value="'.$pais->getNombre().'" required autocomplete="off"/>' ?>
                </div>
                <div align="center">
                    <input name="register" id="submit" type="submit" value="Registrar" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location='consultarPaises.php'" class="Button2">
                </div>
            </form>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}