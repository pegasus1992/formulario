<?php

/**
 * Permite redirigir a la pagina ingresada.
 * @param type $pagina
 */
function redirigir($pagina) {
    header("location:" . $pagina);
}

/**
 * Permite despliegar en el navegador el mensaje ingresado.
 * @param type $mensaje
 * @return type
 */
function mostrarMensaje($mensaje) {
    return "<script language='javascript'>alert('" . $mensaje . "');</script>";
}

/**
 * Permite cargar los datos de sesion.
 */
function initSession() {
    if(!isset($_SESSION)) { 
        session_start(); 
    }
}

/**
 * Convierte un arreglo en String.
 * @param type $array
 * @return string
 */
function array2String($array) {
    $response = "";
    for($i=0; $i<sizeof($array); $i++) {
        $response .= $array[$i];
        if($i < sizeof($array)-1) {
            $response .= ", ";
        }
    }
    return $response;
}

/**
 * Devuelve un campo de imagen en HTML, dado el id de la imagen.
 * @param type $id Id del campo de imagen HTML.
 * @param type $fotoId Id de la imagen.
 * @return type
 */
function imageField($id, $fotoId = null) {
    if($fotoId == null) {
        return '<img id="'.$id.'"/>';
    } else {
        return '<img id="'.$id.'" src="imagen.php?id='.$fotoId.'"/>';
    }
}

function replaceJson2Html($json) {
    return str_replace('"', "'", $json);
}

function replaceHtml2Json($json) {
    return str_replace("'", '"', $json);
}